const fruit = ['apple', 'banana', 'durian', 'pineapple']
console.log(fruit[2])

const tastyFruit = fruit.filter((elem) => {
  if (elem != 'durian') {
    return true
  }
  return false
})

for (let i = 0; i < tastyFruit.length; i++) {
  tastyFruit[i] += 's'
}

console.log(tastyFruit)