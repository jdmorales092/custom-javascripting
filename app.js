const fs = require('fs')
const express = require('express')
const colors = require('colors')
const marked = require('marked')

const app = express()

app.set('view engine', 'ejs')

app.use('/css', express.static('css'))

app.get('/*', (req, res) => {
  const splitPath = req.path.split('/').filter((item) => {
    if (item === '') return false
    return true
  })
  splitPath.push('README.md')
  const path = splitPath.join('/')
  fs.readFile(path, (err, data) => {
    if (err) {
      return res.end('Error!')
    }
    markdown = marked.parse(data.toString())
    res.render('index', { markdown })
  })
})

app.listen(3000, (err) => {
  if (err) {
    return console.log(colors.red('Could not start the server :('))
  }
  console.log(colors.green('Listening on http://localhost:3000 :)'))
})