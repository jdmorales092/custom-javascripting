# El Reto

Crea un objeto llamado __gallardo__ y asígnale las siguientes propiedades:

* `color`: un `String` con valor `'yellow'`.
* `maxSpeed`: un `Number` con valor `320`.
* `amenities`: otro `objeto` que a su vez tiene los valores:
  * `seats`: un `String` con valor `'leather'`.
  * `others`: un `Array` que contiene los siguientes valores: `'Bluetooth Player'`, `'High Accuracy GPS'`

Imprime `'My hopethetical gallardo has the following features:'`, luego recorre el objeto que creaste y si la llave __no__ apunta a un objeto imprime la llave y el valor al que apunta en una nueva línea con este formato: `key: value`, si la llave apunta a un objeto escribe la llave (`key:`), y luego recorre el objeto, imprimiendo llaves y valores igual que antes, indentados __2__ espacios:

```
  key: value
```

__NOTA:__ No imprimas los `Arrays` como `objects`!

# Info

Los objetos (`Objects`) en _JavaScript_, son un tipo de almacenamiento llave valor, a diferencia de un `array`, los valores almacenados se acceden a través de una __llave__, qur también funciona como el __nombre__ de un atributo:

```javascript
const maFruit = {
  name: 'apple',
  color: 'red'
}

const appleColor = maFruit.color

for (let key in maFruit) {
  console.log(key, maFruit[key])
}
```

En el ejemplo definimos un objeto con dos atributos, `name` y `color`, los atributos de un objeto pueden accederse utilizando un `.` y el nombre del atributo o la llave, así, la variable __appleColor__ debería apuntar a un valor de `'red'`.

Podemos recorrer un objeto usando una forma es especial del ciclo `for`, utilizando la __KeyWord__ `in` cada iteración nos entregara una de las llaves del objeto como un `String`, con la cual podemos acceder al valor al que apunta usando la notación de llaves cuadradas (`[]`). Los valores de un objeto pueden ser cualquiera de los tipos de variables que hemos visto, incluso `funciones` u otros `objetos`!

__NOTA:__los `Arrays` son reportados como tipo `object` por `typeof`, si quieres saber si es un array, utiliza `Array.isArray`.
