# El Reto

Declara una variable llamada `oranges` y asígnale el valor de `'oranges'`, en un ciclo agrega signos de exclamación uno a la vez, y ten en cuenta las siguientes condiciones, de acuerdo a cuántos signos de _exclamación_ tiene la variable: si tiene __menos de 3__ imprime `'You are not an orange lover'`, si tiene __entre 3 y 5__ imprime `'You like oranges'`, y si tiene __más de 5__ imprime `'You love oranges!'`, agrega suficiente signos para ver todos los mensajes al menos una vez.

# Info

## Condicionales

Los _condicionales_, como su nombre lo dice nos permiten evaluar condiciones, y tomar una decisión de acuerdo a las mismas, evalúan premisas __Booleanas__ (verdadero o falso):

```javascript
if (a > b) {
  console.log('a is greater than b')
} else if (a == b) {
  console.log('a is equal to b')
} else {
  console.log('b is greater than a')
}
```

En el ejemplo anterior se evalúan condiciones de igualdad entre los números `a` y `b`, si `a` es mayor que `b` se imprime `'a is greater than b'`, si `a` es igual a `b` se imprime `'a is equal to b'`, y en cualquier otro caso `'b is greater than a'`.

## Ciclos

Los _loops_, o _ciclos_ nos ayudan a hacer tareas repetitivas, por ejemplo una misma operación sobre múltiples datos, se declaran de la siguiente manera:

```javascript
for (let i = 0; i < 10; i++) {
  console.log(i)
}
```

La primera parte (antes del primer `;`) declara una variable y la inicializa en un valor (en este caso `0`), la segunda determina una condición para la cuál se ejecuta (el _loop_ solo se ejecuta mientras `i` se _menor_ a `10`), finalmente la tercera parte determina una operación sobre la variable (en este caso se incremente `i` en `1`). En el ejemplo se imprime `i` diez veces, cada vez con un valor diferente, empezando en `0` y terminando en `9`.