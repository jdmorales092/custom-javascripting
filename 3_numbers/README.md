# El Reto

Declara una variable llamada `slices` y asígnale el valor de `5`, e imprime su valor, luego súmale `2.66`, no crees una nueva variable, asigna el resultado de la operación a `slices`, imprime de nuevo la variable, ahora redondea el resultado, de nuevo no crees una nueva variable, ahora imprime el __tipo__ de `slices`, crea una nueva variable llamada `slicesStr`, asígnale el valor de `slices` convertido en `String`, imprime el __tipo__ de la nueva variable. Finalmente concatena `'I want this many slices of pizza:'` con alguna de las variables de manera que contenga `'I want this many slices of pizza: 8'` e imprímelo (puedes usar una variable o imprimirlo directamente).

# Info

Los números en _JavaScript_ se declaran, de nuevo como una variable cualquiera:

```javascript
const a = 21
const b = 11.33
const c = Math.round(b)
const dif = a - c
const difStr = sum.toString()
```

En el ejemplo podemos ver que se pueden realizar operaciones artiméticas básicas, y que la biblioteca `Math` provee funciones para operaciones más avanzadas, así, `dif` apunta a un valor de `10`, `toString` nos permite transformar otros tipos de datos a `Strings`, así, `difStr` apunta a un valor de `'10'`.

Cómo unformación adicional, _JavaScript_ provee la utilidad `typeof` para saber el tipo de una variable.