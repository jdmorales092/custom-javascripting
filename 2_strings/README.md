# El Reto

Declara una variable que se llame `lie`, y asígnale el valor de `'pizza is alright'`, imprime por salida estándar la cadena, y luego el número de caracteres de la cadena, luego crea una variable que se llame `truth`, y partiendo de de la primera variable asígnale el valor de `'pizza is wonderful'`, no se vale simplemente asignar el valor (`let truth = 'pizza is wonderful'`), debes reemplazar la palabra __alright__ en la primera variable y asignar el nuevo valor a `truth`, finalmente imprime el valor y el número de caracteres de `truth`.

# Info

## Strings

Los `Strings` o __cadenas de caracteres__ , son, como su nuombre lo indica cadenas de _0_ o más caracteres (`''` también es un `String`), y como hemos visto anteriormente se pueden declarar como cualquier otra variable:

```javascript
const maString = 'abc'
const maStringLength = maString.length
const maBand = maString.replace('c', 'b') + 'a'
```

En el ejemplo anterior podemos ver que los `Strings` tienen pre-incorporados _métodos_ y _atributos_ utiles para su procesamiento, por ejemplo, el atributo `length` nos retorna el número de caracteres de un `String` (__maStringLength__ debería a apuntar a un `Number` con valor de `4`), y el método `replace` nos permite cambiar una parte de la cadena por otra cadena, nos retorna una copia de la cadena original con el/los reemplazo deseado, además de ello podemos __concatenar__ `Strings` usando el símbolo `+`, así, la variable `maBand` apunta al valor `'abba'`.
